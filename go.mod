module gitlab.com/soockee/tictacgoe

go 1.19

require (
	github.com/eihigh/canvas v0.0.2
	github.com/hajimehoshi/ebiten/v2 v2.4.3
	github.com/rs/zerolog v1.28.0
	github.com/spf13/viper v1.13.0
	golang.org/x/image v0.0.0-20220902085622-e7cb96979f69
)

require (
	github.com/ByteArena/poly2tri-go v0.0.0-20170716161910-d102ad91854f // indirect
	github.com/benoitkugler/textlayout v0.1.3 // indirect
	github.com/dsnet/compress v0.0.1 // indirect
	github.com/ebitengine/purego v0.0.0-20220905075623-aeed57cda744 // indirect
	github.com/flopp/go-findfont v0.1.0 // indirect
	github.com/fsnotify/fsnotify v1.5.4 // indirect
	github.com/go-gl/glfw/v3.3/glfw v0.0.0-20220806181222-55e207c401ad // indirect
	github.com/hajimehoshi/file2byteslice v0.0.0-20210813153925-5340248a8f41 // indirect
	github.com/hashicorp/hcl v1.0.0 // indirect
	github.com/jezek/xgb v1.0.1 // indirect
	github.com/magiconair/properties v1.8.6 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.16 // indirect
	github.com/mitchellh/mapstructure v1.5.0 // indirect
	github.com/pelletier/go-toml v1.9.5 // indirect
	github.com/pelletier/go-toml/v2 v2.0.5 // indirect
	github.com/spf13/afero v1.8.2 // indirect
	github.com/spf13/cast v1.5.0 // indirect
	github.com/spf13/jwalterweatherman v1.1.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/subosito/gotenv v1.4.1 // indirect
	github.com/tdewolff/minify/v2 v2.11.10 // indirect
	github.com/tdewolff/parse/v2 v2.6.0 // indirect
	golang.org/x/exp v0.0.0-20210722180016-6781d3edade3 // indirect
	golang.org/x/mobile v0.0.0-20220722155234-aaac322e2105 // indirect
	golang.org/x/sys v0.0.0-20220915200043-7b5979e65e41 // indirect
	golang.org/x/text v0.3.7 // indirect
	gopkg.in/ini.v1 v1.67.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
