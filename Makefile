ROOT_DIR := $(dir $(realpath $(lastword $(MAKEFILE_LIST))))
ifdef OS
   RM = del /Q
   FixPath = $(subst /,\,$1)
else
   ifeq ($(shell uname), Linux)
      RM = rm -f
      FixPath = $1
   endif
endif

.PHONY: all build build-windows-amd64 build-linux-amd64 test clean run-windows-amd64

all: build

build: build-windows-amd64 build-linux-amd64

build-windows-amd64:
	docker-compose run build-windows-amd64

build-linux-amd64:
	docker-compose run build-linux-amd64

run-windows-amd64:
	cmd /K %cd%/build/windows-amd64-tictacgoe.exe

test:
	docker-compose run test

help:
	docker-compose run help

clean:
	$(RM) $(call FixPath,$(ROOT_DIR)build/*)

